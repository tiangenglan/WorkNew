﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;
using GongZuo.Framework;
using GongZuo.Model;
using GongZuo.Common;

namespace GongZuo.Service.Controllers
{
  public   class RoleController:BaseController
    {
        /// <summary>
        /// 获取单个角色
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string Get(int id)
        {
            var role = Db.Queryable<t_role>().Where(it=>it.id==id).First();
            return role.JsonSerialize();
        }
        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <returns></returns>
        public string GetList()
        {
            int page = Request["page"].TryToInt(1);
            int limit = Request["limit"].TryToInt(10);
            string nm = Request["nm"];
            int totalCount = 0;

            var query = Db.Queryable<t_role>().WhereIF(!string.IsNullOrWhiteSpace(nm), u => u.role_nm.Contains(nm));
            List<t_role> list = null;
            if (page > 0)
            {
                list = query.ToPageList(page, limit, ref totalCount);
            }
            else
            {
                list = query.ToList();
            }
            return new PagerData()
            {
                data = list,
                count = totalCount
            }.JsonSerialize();
        }

        public string Save(string strModel)
        {
            t_role role = strModel.JsonDeserialize<t_role>();
            ResInfo res = new ResInfo();
            res.state = 0;
            if (0 == role.id)
            {
                int role_id = Db.Queryable<t_role>().Max<Int32>(it => it.role_id) + 1;
                role.role_id = role_id;
                if (Db.Queryable<t_role>().Where(it => it.role_nm == role.role_nm).Count() > 0)
                {
                    res.msg = "角色名重复,请重新添加!";
                }
                else if (Db.Insertable(role).ExecuteCommand() > 0)
                {
                    res.state = 1;
                    res.msg = "添加成功!";
                }
                else
                {
                    res.msg = "添加失败!";
                }
            }
            else
            {
                if (Db.Queryable<t_role>().Where(it => it.id != role.id && it.role_nm == role.role_nm).Count() > 0)
                {
                    res.msg = "角色名重复,请重新修改!";
                }
                else if (Db.Updateable(role).UpdateColumns(it => new { it.role_nm, it.detail, it.num }).Where(it => it.id == role.id).ExecuteCommand() > 0)
                {
                    res.state = 1;
                    res.msg = "修改成功!";

                }
                else
                {
                    res.msg = "修改失败!";
                }
            }
            return res.JsonSerialize();

        }

        /// <summary>
        /// 角色删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string Delete(int id)
        {
            ResInfo res = new ResInfo();
            res.state = 0;
            if (1 == id)
            {
                res.msg = "此角色禁止删除";
            }
            else if (Db.Deleteable<t_role>().Where(u => u.id == id).ExecuteCommand() > 0)
            {
                res.state = 1;
                res.msg = "删除成功";
            }
            else
            {
                res.msg = "删除失败";
            }
            return res.JsonSerialize();
        }

        /// <summary>
        /// 获取某个角色的权限
        /// </summary>
        /// <param name="rid"></param>
        /// <returns></returns>
        public string GetPermission(int rid)
        {
            return Db.Queryable<t_permission>()
                .WhereIF(rid > 0, " qx_id in(select permissionid from t_role_permission where roleid ='" + rid + "')")
                .OrderBy(it => it.pid).OrderBy(it => it.num).ToList().JsonSerialize();
        }
        /// <summary>
        /// 角色权限保存
        /// </summary>
        /// <param name="rid"></param>
        /// <param name="strPermissionList"></param>
        /// <returns></returns>
        public string PermissionSave(int rid,string strPermissionList)
        {
            ResInfo res = new ResInfo();
            res.state = 0;
            SqlSugarClient dbt = Db.GetSqlSugarClient();
            try
            {
                dbt.Ado.BeginTran();
                dbt.Deleteable<t_role_permission>().Where(t=>t.roleid==rid).ExecuteCommand();

                List<t_role_permission> list = strPermissionList.JsonDeserialize<List<t_role_permission>>();
                dbt.Insertable(list.ToArray()).ExecuteCommand();
                dbt.Ado.CommitTran();
                res.state = 1;
                res.msg = "保存成功";
            }
            catch (Exception e)
            {
                dbt.Ado.RollbackTran();
                res.state = 0;
                res.msg = "保存失败,"+e.Message.Replace(",",", ");
            }
            return res.JsonSerialize();
        }
    }
}
