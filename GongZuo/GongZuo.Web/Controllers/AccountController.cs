﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GongZuo.Framework;
using System.Drawing;
using GongZuo.Model;
using GongZuo.Common;

namespace GongZuo.Web.Controllers
{ 
    [LoginCheckFilter(IsCheck =false)]
    public class AccountController : BaseController
    {
        /// <summary>
        /// 登录
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            return View();
        }
        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            return View();
        }


        public string CheckLogin(string name,string pwd,string yzm)
        {
            yzm = yzm.ToUpper();
            ResInfo res = new ResInfo() { state = 0 };

            if (Session["YzmCode"].TryToString()!=yzm)
            {
                res.msg = "验证码错误,请重新输入";
                return res.JsonSerialize();
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                res.msg = "账号为空,请重新输入!";
                return res.JsonSerialize();

            }
            pwd = Common.Utils.EncryptDES(pwd);

            t_user user = Db.Queryable<t_user>().Where(u => u.name == name&&u.pwd==pwd).First() ;
            if (null==user)
            {
                res.msg = "用户名或密码错误";
                return res.JsonSerialize();
            }
            return res.JsonSerialize();
        }
        public FileContentResult YzmImage()
        {
            string code;
            string checkCode = String.Empty;
            System.Random ran = new Random();
            string str = @"23456789ABCDEFGHJKMNPQRSTUVWXYZ";
            for (int i = 0; i < 1; i++)
            {
                code = str.Substring(ran.Next(str.Length), 1);
                checkCode += code;
            }



            //获得随机的验证码
            string YzmCode = checkCode.ToUpper();
            //添加验证码到session
            Session["YzmCode"] = YzmCode;

            //制作并输出验证码图片
            Bitmap image = new Bitmap((int)Math.Ceiling(80.0), 24);//50 20// new Bitmap((int)Math.Ceiling((YzmCode.Length * 15.0)), 24);//50 20
            Graphics g = Graphics.FromImage(image);
            try
            {
                //生成随机生成器
                Random random = new Random();

                //清空图片背景色
                g.Clear(Color.White);

                //画图片的背景噪音线
                for (int i = 0; i < 10; i++)
                {
                    int x1 = random.Next(image.Width);
                    int x2 = random.Next(image.Width);
                    int y1 = random.Next(image.Height);
                    int y2 = random.Next(image.Height);
                    g.DrawLine(new Pen(Color.Silver), x1, y1, x2, y2);
                }

                Font font = new System.Drawing.Font("Arial", 14, (System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic));
                System.Drawing.Drawing2D.LinearGradientBrush brush = new System.Drawing.Drawing2D.LinearGradientBrush(new Rectangle(0, 0, image.Width, image.Height), Color.Gray, Color.DarkBlue, 1.2f, true);
                g.DrawString(YzmCode, font, brush, 2, 2);

                //画图片的前景噪音点
                for (int i = 0; i < 40; i++)
                {
                    int x = random.Next(image.Width);
                    int y = random.Next(image.Height);

                    image.SetPixel(x, y, Color.FromArgb(random.Next()));
                }

                //画图片的边框线
                g.DrawRectangle(new Pen(Color.Silver), 0, 0, image.Width - 1, image.Height - 1);

                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);

                return File(ms.ToArray(), "image/gif");

            }
            finally
            {
                g.Dispose();
                image.Dispose();
            }
        }
    }
}