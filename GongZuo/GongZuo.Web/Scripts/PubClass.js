﻿
//使用正则表达式去掉日期的首位0字符
//x日期  y时间格式
function DateRomoveZero(x, y) {
    var z = { M: x.getMonth() + 1, d: x.getDate(), h: x.getHours(), m: x.getMinutes(), s: x.getSeconds() };
    y = y.replace(/(M+|d+|h+|m+|s+)/g, function (v) { return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2) });
    return y.replace(/(y+)/g, function (v) { return x.getFullYear().toString().slice(-v.length) });

}
//验证日期格式的字符串
//date 日期
function TestDate(date) {
    var regex = new RegExp("^(?:(?:([0-9]{4}(-|\/)(?:(?:0?[1,3-9]|1[0-2])(-|\/)(?:29|30)|((?:0?[13578]|1[02])(-|\/)31)))|([0-9]{4}(-|\/)(?:0?[1-9]|1[0-2])(-|\/)(?:0?[1-9]|1\\d|2[0-8]))|(((?:(\\d\\d(?:0[48]|[2468][048]|[13579][26]))|(?:0[48]00|[2468][048]00|[13579][26]00))(-|\/)0?2(-|\/)29))))$");
    if (!regex.test(date)) {
        alert("输入日期有误！");
        // $("#mytext").select();
        return;
    } else {
        alert("日期格式正确！");
    }
}
//判断是否是数字
//str 字符串
function IsNum(str) {
    var reg = /^[0-9]+(.[0-9]+)?$/; //用来验证数字，包括小数的正则表达式
    if (!reg.test(str)) {
        alert(str + "不是正确的数字格式！");
    }
}
//提取字符串中的数字
//str 字符串
function GetNum(str) {
    //注意g将全文匹配，不加将永远只返回第一个匹配 
    var myreg = /[0-9]+/g;
    var myinfo = str.match(myreg);
    return myinfo;
}
//清除首位空格
function RemoveTrim(str) {
    var myinfo = str.replace(/^( |[\s　])+|( |[\s　])+$/g, "");
    return myinfo;
}
//清除标点符号
function RemoveStr(str) {
    //var reg = /[^a-zA-Z\d\u4e00-\u9fa5,.!?()，。．；;？“”；]/g;
    var reg = /[^a-zA-Z\d\u4e00-\u9fa5,.!]/g;
    var myinfo = str.replace(reg, "");
    return myinfo;
}
//清除重复的内容
function RemoveRepeat(str) {
    var myreg = /(.)(?=.*\1)/g;
    var myresult = myinfo.replace(str, "");
    return myresult;
}
//清除非数字字符
function RemoveString(str) {
    var reg = /[^\w]+/g;
    var myinfo = str.replace(reg, "");
    return myinfo;
}





/** 
 * js bug：解决 javascript 的 小数截取的bug。<br> 
 * bug： alert(parseFloat(0.006).toFixed(2)); 显示 0.00 修改后显示 0.01 
 */
Number.prototype.toFixed = function (d) {
    var s = this + "";
    if (!d)
        d = 0;
    if (s.indexOf(".") == -1)
        s += ".";
    s += new Array(d + 1).join("0");
    if (new RegExp("^(-|\\+)?(\\d+(\\.\\d{0," + (d + 1) + "})?)\\d*$").test(s)) {
        var s = "0" + RegExp.$2, pm = RegExp.$1, a = RegExp.$3.length, b = true;
        if (a == d + 2) {
            a = s.match(/\d/g);
            if (parseInt(a[a.length - 1]) > 4) {
                for (var i = a.length - 2; i >= 0; i--) {
                    a[i] = parseInt(a[i]) + 1;
                    if (a[i] == 10) {
                        a[i] = 0;
                        b = i != 1;
                    } else
                        break;
                }
            }
            s = a.join("").replace(new RegExp("(\\d+)(\\d{" + d + "})\\d$"),
                "$1.$2");
        }
        if (b)
            s = s.substr(1);
        return (pm + s).replace(/\.$/, "");
    }
    return this + "";
};

// 给Number类型增加四则运算  
/** 
 * js bug：js四则运算小数精度丢失的bug修复。例如：1.5451+0.34133 
 * 按理来说应该是等于1.88643,结果JS给计算时居然算成1.88629999999998了 调用方法 
 */
/** 
 * 加法：1.5451+0.34133 的调用方法为:var s = (1.5451).add(0.34133).toFixed(2); 
 * alert((7.1).add(12.00027)+"="+(7.1+12.00027)); 
 */
Number.prototype.add = function (arg) {
    var bit1, bit2, m;
    try {
        bit1 = arg.toString().split(".")[1].length;
    } catch (e) {
        bit1 = 0;
    }
    try {
        bit2 = this.toString().split(".")[1].length;
    } catch (e) {
        bit2 = 0;
    }
    m = Math.pow(10, Math.max(bit1, bit2));
    return (this * m + arg * m) / m;
};
/** 
 * 减法: 1.5451-0.34133 的调用方法为:var s = (1.5451).sub(0.34133); 
 */
Number.prototype.sub = function (arg) {
    var bit1, bit2;
    try {
        bit1 = arg.toString().split(".")[1].length;
    } catch (e) {
        bit1 = 0;
    }
    try {
        bit2 = this.toString().split(".")[1].length;
    } catch (e) {
        bit2 = 0;
    }
    var n = Math.max(bit1, bit2);
    var m = Math.pow(10, n);
    return Number(((this * m - arg * m) / m).toFixed(n));
};
/** 
 * 乘法: 1.5451*0.34133 的调用方法为:var s = (1.5451).mul(0.34133).toFixed(3); 
 */
Number.prototype.mul = function (arg) {
    var bit1, bit2;
    try {
        bit1 = arg.toString().split(".")[1].length;
    } catch (e) {
        bit1 = 0;
    }
    try {
        bit2 = this.toString().split(".")[1].length;
    } catch (e) {
        bit2 = 0;
    }
    var m = bit1 + bit2;
    // var n = (bit1 > bit2) ? bit1 : bit2;  
    return (Number(this.toString().replace(".", ""))
        * Number(arg.toString().replace(".", "")) / Math.pow(10, m));// .toFixed(n);  
};
/** 
 * 除法: 1.5451/0.34133 的调用方法为:var s = (1.5451).div(0.34133).toFixed(3); 
 */
Number.prototype.div = function (arg) {
    var bit1, bit2;
    try {
        bit1 = arg.toString().split(".")[1].length;
    } catch (e) {
        bit1 = 0;
    }
    try {
        bit2 = this.toString().split(".")[1].length;
    } catch (e) {
        bit2 = 0;
    }
    var n = Math.max(bit1, bit2);
    var m = Math.pow(10, n);
    // return (Number(this.toString().replace(".", ""))*m) /  
    // (Number(arg.toString().replace(".", ""))*m);  
    return ((this * m) / (arg * m));
};

// 给String对象增加四则运算  
/** 
 * 加法：1.5451+0.34133 的调用方法为:var s = (1.5451).add(0.34133).toFixed(2); 
 * alert((7.1).add(12.00027)+"="+(7.1+12.00027)); 
 */
String.prototype.add = function (arg) {
    return Number(this).add(arg);
};
/** 
 * 减法: 1.5451-0.34133 的调用方法为:var s = (1.5451).sub(0.34133); 
 */
String.prototype.sub = function (arg) {
    return Number(this).sub(arg);
};
/** 
 * 乘法: 1.5451*0.34133 的调用方法为:var s = (1.5451).mul(0.34133).toFixed(3); 
 */
String.prototype.mul = function (arg) {
    return Number(this).mul(arg);
};
/** 
 * 除法: 1.5451/0.34133 的调用方法为:var s = (1.5451).div(0.34133).toFixed(3); 
 */
String.prototype.div = function (arg) {
    return Number(this).div(arg);
};
/** 
 * 处理html转义 调用方法为:var s = "aaa".escape("aaa"); 
 */
String.prototype.escape = function (html) {
    var codeSpan = /(^|[^\\])(`+)([^\r]*?[^`])\2(?!`)/gm;
    var codeBlock = /(?:\n\n|^)((?:(?:[ ]{4}|\t).*\n+)+)(\n*[ ]{0,3}[^ \t\n]|(?=~0))/g;
    var spans = [];
    var blocks = [];
    var text = String(html).replace(/\r\n/g, '\n').replace('/\r/g', '\n');
    text = '\n\n' + text + '\n\n';
    texttext = text.replace(codeSpan, function (code) {
        spans.push(code);
        return '`span`';
    });
    text += '~0';
    return text.replace(codeBlock, function (code) {
        blocks.push(code);
        return '\n\tblock';
    }).replace(/&(?!\w+;)/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/`span`/g, function () {
        return spans.shift();
    }).replace(/\n\tblock/g, function () {
        return blocks.shift();
    }).replace(/~0$/, '').replace(/^\n\n/, '').replace(/\n\n$/, '');
};

// 等比例缩放图片  
//var flag = false;  
/** 
 * ImgD：原图 maxWidth：允许的最大宽度 maxHeight：允许的最大高度 
 */
function resizeimg(ImgD, maxWidth, maxHeight) {
    //ImgD.style.display="none"; //隐藏加到这儿不管用，应该放到img标签中  
    var image = new Image();
    var iwidth = maxWidth; // 定义允许图片宽度  
    var iheight = maxHeight; // 定义允许图片高度  
    image.src = ImgD.src;
    if (image.width > 0 && image.height > 0) {
        //flag = true;  
        if (image.width / image.height >= iwidth / iheight) {
            if (image.width > iwidth) {
                ImgD.width = iwidth;
                ImgD.height = (image.height * iwidth) / image.width;
            } else {
                ImgD.width = image.width;
                ImgD.height = image.height;
            }
            //ImgD.alt = image.width + "×" + image.height;  
        } else {
            if (image.height > iheight) {
                ImgD.height = iheight;
                ImgD.width = (image.width * iheight) / image.height;
            } else {
                ImgD.width = image.width;
                ImgD.height = image.height;
            }
            //ImgD.alt = image.width + "×" + image.height;  
        }
    }
    centerImage(ImgD, maxWidth, maxHeight);
    ImgD.style.display = "inline";
}
/** 
 * Date：给日期对象添加格式化方法 
 * 使用方法 var date = new Date().format('yyyy-MM-dd') 
 */
Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, //month  
        "d+": this.getDate(), //day  
        "h+": this.getHours(), //hour  
        "m+": this.getMinutes(), //minute  
        "s+": this.getSeconds(), //second  
        "q+": Math.floor((this.getMonth() + 3) / 3), //quarter  
        "S": this.getMilliseconds() //millisecond  
    }
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
}

/** 
 * 
 * 扩展 Number 对象 formatMoney()方法（参数：保留小数位数，货币符号，整数部分千位分隔符，小数分隔符）: 
 * 调用方法为:var s = (60000).formatMoney(0,"","","");结果为：s = 60,000 
 */
Number.prototype.formatMoney = function (places, symbol, thousand, decimal) {
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var number = this,
        negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};

function centerImage(imgD, maxWidth, maxHeight) {
    // var div = imgD.parentNode;// 获取包含本图片的div不用这个  
    if (imgD.height < maxHeight) {
        var top = (maxHeight - imgD.height) / 2 - 2;
        //ie6不支持这么获取高度，所以ie6下不执行  
        if (imgD.height != 0) {
            imgD.style.marginTop = top + "px";
        }
    }
    if (imgD.width < maxWidth) {
        var left = (maxWidth - imgD.width) / 2;
        //ie6不支持这么获取宽度，所以ie6下不执行  
        if (imgD.width != 0) {
            imgD.style.marginLeft = left + "px";
        }
    }
}
/** 
 * 将字符串格式的日期（如：20150415092645）转换成js Date对象 
 * 注意Date对象的初始化方式 
 * */
function js_patch_getdate(stime) {
    var datetime = new Date(
        parseFloat(stime.substr(0, 4)),
        parseFloat(stime.substr(4, 2) - 1),
        parseFloat(stime.substr(6, 2)),
        parseFloat(stime.substr(8, 2)),
        parseFloat(stime.substr(10, 2)),
        parseFloat(stime.substr(12, 2)));
    return datetime;
}
function js_patch_fillZero(num, digit) {
    var str = '' + num;
    while (str.length < digit) {
        str = '0' + str;
    }
    return str;
}
function debug_showAttr(obj) {
    var str = "<b>begin:the attribute of " + obj + "</b><br/>";
    for (var i in obj) {
        str += "<b>" + i + "</b>==" + obj[i] + "<br>";
    }
    document.body.innerHTML += str;
}
function debug_showMsg(str) {
    document.body.innerHTML += str;
}
function debug_showMsgSrc(str) {
    document.body.innerHTML += str.replace(/</g, "&lt").replace(/>/g, "&gt");
}



/*
* 以某字符串开头判断
*注意:参数str中如果包含正则通配字符,如"."请改参数为"\\."
*/
String.prototype.startWith = function (str) {
    var reg = new RegExp("^" + str);
    return reg.test(this);
}

/*
* 以某字符串结尾判断
*注意:参数str中如果包含正则通配字符,如"."请改参数为"\\."
*/
String.prototype.endWith = function (str) {
    var reg = new RegExp(str + "$");
    return reg.test(this);
}

/*
*字符串去除两侧空格或指定字符
*注意:参数str中如果包含正则通配字符,如"."请改参数为"\\."
*/
String.prototype.trimAll = function (str) {//js1.8版本以上自带trim,这里改名强烈建议不重复构写方法
    str = (str ? str : "\\s");
    str = ("(" + str + ")");
    var reg_trim = new RegExp("(^" + str + "*)|(" + str + "*$)", "g");
    return this.replace(reg_trim, "");
};
/*--------------------------------String扩展----------------------------------------------------------------------------*/
/*
*字符串去除左侧空格或指定字符
*注意:参数str中如果包含正则通配字符,如"."请改参数为"\\."
*/
String.prototype.trimLeft = function (str) {
    str = (str ? str : "\\s");                            //没有传入参数的，默认去空格
    str = ("(" + str + ")");
    var reg_lTrim = new RegExp("^" + str + "*", "g");     //拼正则
    return this.replace(reg_lTrim, "");
}

/*
*字符串去除右侧空格或指定字符
*注意:参数str中如果包含正则通配字符,如"."请改参数为"\\."
*/
String.prototype.trimRight = function (str) {
    str = (str ? str : "\\s");
    str = ("(" + str + ")");
    var reg_rTrim = new RegExp(str + "*$", "g");
    return this.replace(reg_rTrim, "");
}

/*
*字符串是否包含某字符串
*/
String.prototype.contains = function (str) {//利用indexof  
    //值为位置,为-1说明不包含  
    if (this.indexOf(str) >= 0) return true;
    else return false;
};

/*
*字符串是否是大于0的整数或小数
*/
String.prototype.isPositiveDecimal = function () {
    var reg = /^((0\.\d*[1-9])|([1-9]\d*(\.\d*[1-9])?))$/;
    return reg.test(this);
}
/*是否包含contains*/

String.prototype.contains = function (str) {//利用indexof
    //值为位置,为-1说明不包含
    if (this.indexOf(str) >= 0) return true;
    else return false;
};

String.prototype.contains = function (str) {//利用正则表达式
    var reg = new RegExp(str);
    return reg.test(this);
};






//获取字符数组
String.prototype.toCharArray = function () {
    return this.split("");
}
//获取N个相同的字符串
String.prototype.repeat = function (num) {
    var tmpArr = [];
    for (var i = 0; i < num; i++) {
        temArr.push(this);
        return temArr.join("");
    }
}
//逆序
String.prototype.reverse = function () {
    return this.split("").reverse().join("");

}
//测试是否是数字
String.prototype.isNumeric = function () {
    var tmpFloat = parseFloat(this);
    if (isNaN(tmpFloat))
        return false;
    var tmpLen = this.length - tmpFloat.toString().length;
    return tmpFloat + "0".Repeat(tmpLen) == this;
}
//测试是否是整数
String.prototype.isInt = function () {
    if (this == "NaN")
        return false;
    return this == parseInt(this).toString();
}
// 合并多个空白为一个空白
String.prototype.resetBlank = function () {
    return this.replace(/s+/g, " ");
}
// 除去左边空白
String.prototype.LTrim = function () {
    return this.replace(/^s+/g, "");
}
// 除去右边空白
String.prototype.RTrim = function () {
    return this.replace(/s+$/g, "");
}
// 除去两边空白
String.prototype.trim = function () {
    return this.replace(/(^s+)|(s+$)/g, "");
}
// 保留数字
String.prototype.getNum = function () {
    return this.replace(/[^d]/g, "");
}
// 保留字母
String.prototype.getEn = function () {
    return this.replace(/[^A-Za-z]/g, "");
}
// 保留中文
String.prototype.getCn = function () {
    return this.replace(/[^u4e00-u9fa5uf900-ufa2d]/g, "");
}
// 得到字节长度
String.prototype.getRealLength = function () {
    return this.replace(/[^x00-xff]/g, "--").length;
}
// 从左截取指定长度的字串
String.prototype.left = function (n) {
    return this.slice(0, n);
}
// 从右截取指定长度的字串
String.prototype.right = function (n) {
    return this.slice(this.length - n);
}
// HTML编码
String.prototype.HTMLEncode = function () {
    var re = this;
    var q1 = [/x26/g, /x3C/g, /x3E/g, /x20/g];
    var q2 = ["&", "<", ">", " "];
    for (var i = 0; i < q1.length; i++)
        re = re.replace(q1[i], q2[i]);
    return re;
}
// Unicode转化
String.prototype.ascW = function () {
    var strText = "";
    for (var i = 0; i < this.length; i++)
        strText += "&#" + this.charCodeAt(i) + ";";
    return strText;
}
// 获取文件全名
String.prototype.GetFileName = function () {
    var regEx = /^.*\/([^\/\?]*).*$/;
    return this.replace(regEx, '$1');
};
// 获取文件扩展名
String.prototype.GetExtensionName = function () {
    var regEx = /^.*\/[^\/]*(\.[^\.\?]*).*$/;
    return this.replace(regEx, '$1');
};
//替换所有
String.prototype.replaceAll = function (reallyDo, replaceWith, ignoreCase) {
    if (!RegExp.prototype.isPrototypeOf(reallyDo)) {
        return this.replace(new RegExp(reallyDo, (ignoreCase ? "gi" : "g")), replaceWith);
    } else {
        return this.replace(reallyDo, replaceWith);
    }
};
//格式化字符串
String.prototype.Format = function () {
    if (arguments.length == 0) {
        return '';
    }
    if (arguments.length == 1) {
        return arguments[0];
    }
    var reg = /{(\d+)?}/g;
    var args = arguments;
    var result = arguments[0].replace(reg, function ($0, $1) {
        return args[parseInt($1) + 1];
    });
    return result;
};

// 数字补零
Number.prototype.LenWithZero = function (oCount) {
    var strText = this.toString();
    while (strText.length < oCount) {
        strText = '0' + strText;
    }
    return strText;
};
// Unicode还原
Number.prototype.ChrW = function () {
    return String.fromCharCode(this);
};


// 数字数组由小到大排序
Array.prototype.Min2Max = function () {
    var oValue;
    for (var i = 0; i < this.length; i++) {
        for (var j = 0; j <= i; j++) {
            if (this[i] < this[j]) {
                oValue = this[i];
                this[i] = this[j];
                this[j] = oValue;
            }
        }
    }
    return this;
};

// 数字数组由大到小排序
Array.prototype.Max2Min = function () {
    var oValue;
    for (var i = 0; i < this.length; i++) {
        for (var j = 0; j <= i; j++) {
            if (this[i] > this[j]) {
                oValue = this[i];
                this[i] = this[j];
                this[j] = oValue;
            }
        }
    }
    return this;
};

// 获得数字数组中最大项
Array.prototype.GetMax = function () {
    var oValue = 0;
    for (var i = 0; i < this.length; i++) {
        if (this[i] > oValue) {
            oValue = this[i];
        }
    }
    return oValue;
};

// 获得数字数组中最小项
Array.prototype.GetMin = function () {
    var oValue = 0;
    for (var i = 0; i < this.length; i++) {
        if (this[i] < oValue) {
            oValue = this[i];
        }
    }
    return oValue;
};

Array.prototype.add = function (item) {
    this.push(item);
}
Array.prototype.addRange = function (items) {
    var length = items.length;
    if (length != 0) {
        for (var index = 0; index < length; index++) {
            this.push(items[index]);

        }
    }
}
Array.prototype.clear = function () {
    if (this.length > 0) {
        this.splice(0, this.length);
    }
}
Array.prototype.isEmpty = function () {
    if (this.length == 0) {
        return true;
    }
    else {
        return false;
    }
}
Array.prototype.clone = function () {
    var clonedArray = [];
    var length = this.length;
    for (var index = 0; index < length; index++) {
        clonedArray[index] = this[index];
    }
    return clonedArray;
}
Array.prototype.contains = function (item) {
    var index = this.indexOf(item);
    return (index >= 0);
}
Array.prototype.dequeue = function () {
    return this.shift();
}
Array.prototype.indexOf = function (item) {
    var length = this.length;

    if (length != 0) {
        for (var index = 0; index < length; index++) {
            if (this[index] == item) {
                return index;
            }
        }
    }
    return -1;
}
Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
}
Array.prototype.joinstr = function (str) {
    var newStr = new Array(this.length);
    for (var i = 0; i < this.length; i++) {
        newStr[i] = this[i] + str;
    }
    return newStr;
}
Array.prototype.queue = function (item) {//入队
    this.push(item);
}
Array.prototype.remove = function (item) {
    var index = this.indexOf(item);
    if (index >= 0) {
        this.splice(index, 1);
    }
}
Array.prototype.removeAt = function (index) {
    this.splice(index, 1);
}
//给js原生Array增加each方法
Array.prototype.each = function (fn) {
    return this.length ? [fn(this.slice(0, 1))].concat(this.slice(1).each(fn)) : [];
};

// 获取当前时间的中文形式
Date.prototype.GetCNDate = function () {
    var oDateText = '';
    oDateText += this.getFullYear().LenWithZero(4) + new Number(24180).ChrW();
    oDateText += this.getMonth().LenWithZero(2) + new Number(26376).ChrW();
    oDateText += this.getDate().LenWithZero(2) + new Number(26085).ChrW();
    oDateText += this.getHours().LenWithZero(2) + new Number(26102).ChrW();
    oDateText += this.getMinutes().LenWithZero(2) + new Number(20998).ChrW();
    oDateText += this.getSeconds().LenWithZero(2) + new Number(31186).ChrW();
    oDateText += new Number(32).ChrW() + new Number(32).ChrW() + new Number(26143).ChrW() + new Number(26399).ChrW() + new String('26085199682010819977222352011620845').substr(this.getDay() * 5, 5).ToInt().ChrW();
    return oDateText;
};
//扩展Date格式化
Date.prototype.Format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours() % 12 == 0 ? 12 : this.getHours() % 12, //小时
        "H+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    var week = {
        "0": "\u65e5",
        "1": "\u4e00",
        "2": "\u4e8c",
        "3": "\u4e09",
        "4": "\u56db",
        "5": "\u4e94",
        "6": "\u516d"
    };
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    if (/(E+)/.test(format)) {
        format = format.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "\u661f\u671f" : "\u5468") : "") + week[this.getDay() + ""]);
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return format;
}
Date.prototype.Diff = function (interval, objDate) {
    //若参数不足或 objDate 不是日期类型則回传 undefined
    if (arguments.length < 2 || objDate.constructor != Date) { return undefined; }
    switch (interval) {
        //计算秒差
        case 's': return parseInt((objDate - this) / 1000);
        //计算分差
        case 'n': return parseInt((objDate - this) / 60000);
        //计算時差
        case 'h': return parseInt((objDate - this) / 3600000);
        //计算日差
        case 'd': return parseInt((objDate - this) / 86400000);
        //计算周差
        case 'w': return parseInt((objDate - this) / (86400000 * 7));
        //计算月差
        case 'm': return (objDate.getMonth() + 1) + ((objDate.getFullYear() - this.getFullYear()) * 12) - (this.getMonth() + 1);
        //计算年差
        case 'y': return objDate.getFullYear() - this.getFullYear();
        //输入有误
        default: return undefined;
    }
};

//检测是否为空
Object.prototype.IsNullOrEmpty = function () {
    var obj = this;
    var flag = false;
    if (obj == null || obj == undefined || typeof (obj) == 'undefined' || obj == '') {
        flag = true;
    } else if (typeof (obj) == 'string') {
        obj = obj.trim();
        if (obj == '') {//为空
            flag = true;
        } else {//不为空
            obj = obj.toUpperCase();
            if (obj == 'NULL' || obj == 'UNDEFINED' || obj == '{}') {
                flag = true;
            }
        }
    }
    else {
        flag = false;
    }
    return flag;
}
   

