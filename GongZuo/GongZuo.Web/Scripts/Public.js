﻿//全局变量
var Public = Public || {};
Public.width = $(window).width();
Public.pageSize = 10;
Public.pageIndex = 1;
Public.baseUrl = "/";

//时间戳转日期
Number.prototype.ToDate = String.prototype.ToDate = function (format) {
    //时间戳为0时 ,js按得8点 ,所以从后台得到的值 再减去8小时 才是js的0点
    return new Date((this - 28800) * 1000);
}

//判断是否为空
$.isNull = $.isnull = $.IsNull = function (val) {
    if ("" == val || null == val || typeof (val) == "undefined") {
        return true;
    }
    else {
        return false;
    }
}

String.prototype.isNull = function () {
    return $.IsNull(this.Tirm());
}
String.prototype.isnull = function () {
    return $.IsNull(this.Trim());
}
String.prototype.IsNull = function () {
    return $.IsNull(this.Trim());
}
//序列化
String.prototype.parse = function () {
    return JSON.parse(this);
}
//清除空格
$.Tirm = function (str) {
    var reg = /^(\s*)|(\s*)$/g;
    return String(str).replace(re,'');
}

String.prototype.Trim = function () {
    return this.replace(/(^\s*)|(\s*$)/g, "");
}
//Html编码
function htmlEncode(value) {
    return $('<div/>').text(value).html();
}
//Html解码获取Html实体
function htmlDecode(value) {
    return $('<div/>').html(value).text();
}
//写日志
function log(str) {
    try {
        console.log(str);
    } catch (e) {
        
    }
}


//尝试保留小数 默认2位
function tryToFixed(value, xsw) {
    var v = parseFloat(value);
    if (isNaN(v) || -99999 == v) {
        return "";
    }
    else {
        if (typeof (xsw) == "undefined") {
            xsw = 2;
        }
        return v.toFixed(xsw);
    }
}
Number.prototype.tryToFixed = String.prototype.tryToFixed = function (xsw) {
    return tryToFixed(this, xsw);
}

//尝试保存日期格式
function tryToDateString(value) {
    value = parseInt(value);
    if (isNaN(value) || value <= 0) {
        return "";
    }
    else {
        //return new Date(value * 1000).format("yyyy-MM-dd");
        return value.ToDate().format("yyyy-MM-dd");
    }
}
//尝试保存日期格式
function tryToDateHourString(value) {
    value = parseInt(value);
    if (isNaN(value) || value <= 0) {
        return "";
    }
    else {
        //return new Date(value * 1000).format("yyyy-MM-dd HH:mm:ss");
        return value.ToDate().format("yyyy-MM-dd HH:mm:ss");
    }
}

$.Ajax = function (ControllerAndAction, para, suncessFun, errorFun) {
    $.ajax({
        url: Public.baseUrl + ControllerAndAction,
        data: para,
        success: function (ref) {
            if ("nullSession" == ref) {
                alert("用户登录状态失效!");
                parent.location = "/Account/Login";
                return;
            }
            if ("noPermision" == ref) {
                alert("无相关权限!");
                parent.location = "/Account/Login";
                return;
            }
            suncessFun(ref);
        },
        error: function (ref) {
            if (typeof (errorFun) == "undefined") {
                alert(ControllerAndAction + " error");
            }
            else {
                //执行错误的回调方法
                errorFun(ref);
            }
        }
    });
}


//简单封装AJAX
$.Ajax2 = function (ControllerAndAction, para, suncessFun, errorFun) {
    $.ajax({
        url: "/" + ControllerAndAction,
        data: para,
        async: false,
        type: 'POST',
        success: function (ref) {

            if ("nullSession" == ref) {
                alert("用户登录状态失效!");
                parent.location = "/Account/Login";
                return;
            }
            if ("noPermision" == ref) {
                alert("无相关权限!");
                parent.location = "/Account/Login";
                return;
            }
            //执行成功的回调方法
            suncessFun(ref);
        },
        error: function (ref) {
            if (typeof (errorFun) == "undefined") {
                alert(ControllerAndAction + " error");
            }
            else {
                //执行错误的回调方法
                errorFun(ref);
            }
        }
    });
}
//简单的alert
$.alert = function (content, iconindex, yes) {
    layer.alert(content, { icon: iconindex, offset: '100px' }, yes);
}
//弹出错误
$.alertE = alertE = function (content, yes) {
    layer.alert(content, { icon: 2, offset: '100px' }, yes);
}
//弹出成功
$.alertS = alertS = function (content, yes) {
    layer.alert(content, { icon: 1, offset: '100px' }, yes);
}
//弹出警告
$.alertA = alertA = function (content, yes) {
    layer.alert(content, { icon: 0, offset: '100px' }, yes);
}
//弹出询问框
$.confirm = function (content, yes, cancel) {
    layer.confirm(content, { icon: 3, title: '提示', offset: '100px' }, function (index) {
        layer.close(index);
        yes();
    }, cancel);
}

//判断邮箱
String.prototype.isEmail = function () {
    return /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/.test(this);
}

//是否是有效的身份证(中国)
String.prototype.isIDCard = function () {
    var iSum = 0;
    var info = "";
    var sId = this;

    var aCity = { 11: "北京", 12: "天津", 13: "河北", 14: "山西", 15: "内蒙古", 21: "辽宁", 22: "吉林", 23: "黑龙江", 31: "上海", 32: "江苏", 33: "浙江", 34: "安徽", 35: "福建", 36: "江西", 37: "山东", 41: "河南", 42: "湖北", 43: "湖南", 44: "广东", 45: "广西", 46: "海南", 50: "重庆", 51: "四川", 52: "贵州", 53: "云南", 54: "西藏", 61: "陕西", 62: "甘肃", 63: "青海", 64: "宁夏", 65: "新疆", 71: "台湾", 81: "香港", 82: "澳门", 91: "国外" };

    if (!/^\d{17}(\d|x)$/i.test(sId)) {
        return false;
    }
    sId = sId.replace(/x$/i, "a");
    //非法地区
    if (aCity[parseInt(sId.substr(0, 2))] == null) {
        return false;
    }

    var sBirthday = sId.substr(6, 4) + "-" + Number(sId.substr(10, 2)) + "-" + Number(sId.substr(12, 2));

    var d = new Date(sBirthday.replace(/-/g, "/"))

    //非法生日
    if (sBirthday != (d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate())) {
        return false;
    }
    for (var i = 17; i >= 0; i--) {
        iSum += (Math.pow(2, i) % 11) * parseInt(sId.charAt(17 - i), 11);
    }

    if (iSum % 11 != 1) {
        return false;
    }
    return true;

}

//是否是数字
String.prototype.isNumeric = function (flag) {
    //验证是否是数字
    if (isNaN(this)) {

        return false;
    }

    switch (flag) {

        case null:        //数字
        case "":
            return true;
        case "+":        //正数
            return /(^\+?|^\d?)\d*\.?\d+$/.test(this);
        case "-":        //负数
            return /^-\d*\.?\d+$/.test(this);
        case "i":        //整数
            return /(^-?|^\+?|\d)\d+$/.test(this);
        case "+i":        //正整数
            return /(^\d+$)|(^\+?\d+$)/.test(this);
        case "-i":        //负整数
            return /^[-]\d+$/.test(this);
        case "f":        //浮点数
            return /(^-?|^\+?|^\d?)\d*\.\d+$/.test(this);
        case "+f":        //正浮点数
            return /(^\+?|^\d?)\d*\.\d+$/.test(this);
        case "-f":        //负浮点数
            return /^[-]\d*\.\d$/.test(this);
        default:        //缺省
            return true;
    }
}

//是否是汉字
String.prototype.CheckChinese = function () {
    var reg = /^[\u0391-\uFFE5]+$/;
    //      [\u4E00-\u9FA5]; 
    return reg.test(this);
}

//是否是手机号码 
String.prototype.IsMobile = function () {
    var reg = /^(13|14|15|18)[0-9]{9}$/;
    return reg.test(this);
}