﻿layui.define(["jquery", "tabbox"], function (exports) {
    var $ = layui.jquery;
   

    //添加三级菜单支持
    $(" .layui-nav-two").hover(function () {
        $(this).find('.layui-nav-two-child').attr('style', 'left:' + $(this).width() + 'px;').addClass("layui-show");
    }, function () {
        $(this).find('.layui-nav-two-child').removeClass("layui-show");
    })
    $('.layui-nav-two-child dd').click(function () {
        $(".layui-this").removeClass("layui-this");
        $(this).addClass("layui-this");
    })

    //添加四级菜单支持
    $(".layui-nav-three").hover(function () {
        $(this).find('.layui-nav-three-child').attr('style', 'left:' + $(this).width() + 'px;').addClass("layui-show");
    }, function () {
        $(this).find('.layui-nav-three-child').removeClass("layui-show");
    })
    $('.layui-nav-three-child dd').click(function () {
        $(".layui-this").removeClass("layui-this");
        $(this).addClass("layui-this");
    })

    //取消一些事件
    $('.layui-nav-two').unbind("click");
    $(".layui-nav-two").click(function (e) {
        $(this).removeClass("layui-this");
        e.preventDefault();
    });
    $('.layui-nav-three').unbind("click");
    $(".layui-nav-three").click(function (e) {
        $(this).removeClass("layui-this");
        e.preventDefault();
    });

    //加载tabbox
    layui.tabbox.set({ openWait: false, mainUrl: MainUrl, mainID: MainID }).render();

    //输出
    exports('home', {}); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致
});