﻿layui.define(["jquery"], function (exports) {
    var $ = layui.jquery;


    //加载行政区
    ShiXzqSelectBind();
    //绑定市更改选择控件
    $("#org").on('change', function () {
        DataBind();
    });
    //绑定重新汇总按钮单击事件
    $("#reportAgain").on('click', function () {
        DataBind();
    });
    //加载报表数据
    DataBind()
    //数据绑定
    function DataBind() {
        var xzq = $("#org").val();
        //开始进行数据绑定
        $.Ajax(dataurl, { xzq: xzq }, function (ref) {
            var title = $("#org").find("option:selected").text() + reportTitle;
            var data = JSON.parse(ref);
            LeftTbBind(cate, data);
            ShowJJReprtChart2(title, cate, data);
        })
    }
     

    //输出
    exports('smReport', {}); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致
});