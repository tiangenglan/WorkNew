﻿layui.define(["jquery", 'form'], function (exports) {
    var $ = layui.jquery;
    var form = layui.form;

    //地图初始化
    function init(defX, defY, defZoom) {

        //创建地图对象
        map = new BMap.Map("container", { enableMapClick: false });

         
        var mapType1 = new BMap.MapTypeControl({ mapTypes: [BMAP_NORMAL_MAP, BMAP_HYBRID_MAP] });
        var mapType2 = new BMap.MapTypeControl({ anchor: BMAP_ANCHOR_TOP_LEFT });
        map.addControl(mapType1);          //2D图，卫星图
        map.addControl(mapType2);          //左上角，默认地图控件
      


        // 百度地图属性设置
        map.enableScrollWheelZoom();    //启用滚轮放大缩小，默认禁用
        map.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用

        //定义地图级别
        lastzoom = map.getZoom().toString();
        map.addEventListener("zoomend", function () {
            var zoom = map.getZoom().toString();
            lastzoom = zoom;
        });

        // 创建中心点坐标  
        var point = new BMap.Point(defX, defY);
        // 初始化地图，设置中心点坐标和地图级别  
        map.centerAndZoom(point, defZoom);

        //遮罩物
        marker = new BMap.Marker(point);// 创建标注
        map.addOverlay(marker);             // 将标注添加到地图中
        marker.enableDragging();

        //标签
        label = new BMap.Label("当前坐标：" + defX + "," + defY, { offset: new BMap.Size(20, 10) });
        marker.setLabel(label);

        //遮罩物拖拽事件dragging
        marker.addEventListener("dragging", getAttr)
        //marker.addEventListener("dragend", getAttr)

        //添加点击事件：
        $("#btnOk").click(function () {
            var p = marker.getPosition();       //获取marker的位置
            parent.SetXY(p.lng, p.lat);
        })
    }

    //获取坐标
    function getAttr() {
        var p = marker.getPosition();       //获取marker的位置
        label.setContent("当前坐标" + p.lng + "," + p.lat);
    }
   
    //输出
    exports('map', { init: init }); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致
});