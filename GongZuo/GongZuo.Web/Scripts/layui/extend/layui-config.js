﻿layui.config({
    base: '/Scripts/layui/lay/modules/' //你存放新模块的目录，注意，不是layui的模块目录
}).extend({ //设定模块别名
    //    mymod: 'mymod' //如果 mymod.js 是在根目录，也可以不用设定别名
    //  , mod1: 'admin/mod1' //相对于上述 base 目录的子目录
    'home': '{/}/Scripts/layui/extend/modules/home', // {/}的意思即代表采用自有路径，即不跟随 base 路径
    'map': '{/}/Scripts/layui/extend/modules/map',
    'formSelects': '{/}/Scripts/layui/extend/modules/formSelects-v4',
    'smReport': '{/}/Scripts/layui/extend/modules/smReport'
})
