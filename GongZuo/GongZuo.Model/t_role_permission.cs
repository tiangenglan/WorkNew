﻿using System;
using System.Linq;
using System.Text;

namespace GongZuo.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class t_role_permission
    {
           public t_role_permission(){


           }
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal id {get;set;}

           /// <summary>
           /// Desc:角色编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? roleid {get;set;}

           /// <summary>
           /// Desc:权限编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? permissionid {get;set;}

    }
}
