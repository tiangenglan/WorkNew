﻿using System;
using System.Linq;
using System.Text;

namespace GongZuo.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class sm_banben
    {
           public sm_banben(){


           }
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal id {get;set;}

           /// <summary>
           /// Desc:部门名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string banben_name {get;set;}

           /// <summary>
           /// Desc:部门负责人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string banben_fuzeren {get;set;}

    }
}
