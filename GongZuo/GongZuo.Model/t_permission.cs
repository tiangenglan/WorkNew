﻿using System;
using System.Linq;
using System.Text;

namespace GongZuo.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class t_permission
    {
           public t_permission(){


           }
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal id {get;set;}

           /// <summary>
           /// Desc:权限编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int per_id {get;set;}

           /// <summary>
           /// Desc:权限名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string per_nm {get;set;}

           /// <summary>
           /// Desc:url地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string url {get;set;}

           /// <summary>
           /// Desc:权限类型 1菜单 2功能
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int type {get;set;}

           /// <summary>
           /// Desc:图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string pic {get;set;}

           /// <summary>
           /// Desc:父编号-上级权限编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? pid {get;set;}

           /// <summary>
           /// Desc:排序编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? num {get;set;}

           /// <summary>
           /// Desc:描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string detail {get;set;}

    }
}
