﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GongZuo.Model
{
    /// <summary>
    /// 返回信息类  正确state 返回1 （ 返回列表数据用 PagerData，里面状态用的是code 正确返回0 为了方便layui）
    /// </summary>
    public class ResInfo
    {
        /// <summary>
        /// 状态
        /// </summary>
        public int state { get; set; }
        /// <summary>
        /// 返回信息
        /// </summary>
        public string msg { get; set; }
        /// <summary>
        /// 返回数据
        /// </summary>
        public object data { get; set; }

    }
}
