﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GongZuo.Model
{
    /// <summary>
    /// 分页数据类
    /// </summary>
   public class PagerData
    {
        /// <summary>
        /// 状态默认 为1
        /// </summary>
        public int code = 0;
        /// <summary>
        /// 总记录数
        /// </summary>
        public int count { get; set; }
        /// <summary>
        /// 列表数据信息
        /// </summary>
        public object data { get; set; }
        /// <summary>
        /// 其他数据  例如合计
        /// </summary>
        public object otherData { get; set; }
        /// <summary>
        /// 包含的消息
        /// </summary>
        public string msg { get; set; }

    }
}
