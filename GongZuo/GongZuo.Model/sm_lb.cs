﻿using System;
using System.Linq;
using System.Text;

namespace GongZuo.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class sm_lb
    {
           public sm_lb(){


           }
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal id {get;set;}

           /// <summary>
           /// Desc:项目类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string lb_name {get;set;}

           /// <summary>
           /// Desc:项目难度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? lb_nandu {get;set;}

    }
}
