﻿using System;
using System.Linq;
using System.Text;

namespace GongZuo.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class log_ip
    {
           public log_ip(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal id {get;set;}

           /// <summary>
           /// Desc:登录名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string login_name {get;set;}

           /// <summary>
           /// Desc:登录IP
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string login_IP {get;set;}

           /// <summary>
           /// Desc:登录时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? login_date {get;set;}

           /// <summary>
           /// Desc:登录部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string login_bm {get;set;}

    }
}
