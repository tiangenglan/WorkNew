﻿using System;
using System.Linq;
using System.Text;

namespace GongZuo.Model
{
    ///<summary>
    ///
    ///</summary>
    public partial class t_role
    {
           public t_role(){


           }
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal id {get;set;}

           /// <summary>
           /// Desc:角色编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int role_id {get;set;}

           /// <summary>
           /// Desc:角色名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string role_nm {get;set;}

           /// <summary>
           /// Desc:角色说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string detail {get;set;}

           /// <summary>
           /// Desc:排序编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? num {get;set;}

    }
}
