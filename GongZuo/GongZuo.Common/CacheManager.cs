﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace GongZuo.Common
{
    /// <summary>
    /// 基于Asp.Net内置缓存的缓存策略
    /// </summary>
    public static class CacheManager
    {
        private static Cache _cache;
        static CacheManager()
        {
            _cache = HttpRuntime.Cache;
        }

        /// <summary>
        /// 获得指定键的缓存值
        /// </summary>
        /// <param name="key">缓存键</param>
        /// <returns>缓存值</returns>
        public static object Get(string key)
        {
            return _cache.Get(key);
        }

        /// <summary>
        /// 将指定键的对象添加到缓存中(1小时)
        /// </summary>
        /// <param name="key">缓存键</param>
        /// <param name="data">缓存值</param>
        public static void Insert(string key, object data)
        {
            int _timeout = 3600;//单位秒
            _cache.Insert(key, data, null, DateTime.Now.AddSeconds(_timeout), Cache.NoSlidingExpiration, CacheItemPriority.High, null);
        }

        /// <summary>
        /// 将指定键的对象添加到缓存中，并指定过期时间
        /// </summary>
        /// <param name="key">缓存键</param>
        /// <param name="data">缓存值</param>
        /// <param name="cacheTime">缓存过期时间(单位秒)</param>
        public static void Insert(string key, object data, int cacheTime)
        {
            _cache.Insert(key, data, null, DateTime.Now.AddSeconds(cacheTime), Cache.NoSlidingExpiration, CacheItemPriority.High, null);
        }

        /// <summary>
        /// 将具有文件依赖项或键依赖项的对象添加到缓存中，并指定过期时间
        /// </summary>
        /// <param name="key">缓存键</param>
        /// <param name="data">缓存值</param>
        /// <param name="cacheDependency">缓存依赖项</param>
        /// <param name="cacheTime">缓存过期时间(单位秒)</param>
        public static void Insert(string key, object data, CacheDependency cacheDependency, int cacheTime)
        {
            _cache.Insert(key, data, cacheDependency, DateTime.Now.AddSeconds(cacheTime), Cache.NoSlidingExpiration, CacheItemPriority.High, null);
        }

        /// <summary>
        /// 从缓存中移除指定键的缓存值
        /// </summary>
        /// <param name="key">缓存键</param>
        public static void Remove(string key)
        {
            _cache.Remove(key);
        }

        /// <summary>
        /// 清空所有缓存对象
        /// </summary>
        public static void Clear()
        {
            IDictionaryEnumerator cacheEnum = _cache.GetEnumerator();
            while (cacheEnum.MoveNext())
                _cache.Remove(cacheEnum.Key.ToString());
        }

        /// <summary>
        /// 清除批量缓存对象
        /// </summary>
        public static void Clear(string key)
        {
            IDictionaryEnumerator cacheEnum = _cache.GetEnumerator();
            while (cacheEnum.MoveNext())
            {
                string cacheKey = cacheEnum.Key.ToString();
                if (cacheKey.Contains(key))
                {
                    _cache.Remove(cacheKey);
                }

            }
        }
    }
}

