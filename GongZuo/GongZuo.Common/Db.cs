﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;

namespace GongZuo.Common
{
   public class Db
    {
        /// <summary>
        /// 初始化数据库
        /// </summary>
        /// <returns></returns>
        public static SqlSugarClient GetSqlSugarClient()
        {

            
            SqlSugarClient Db = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = Utils.GetAppSetting("connString"),
                DbType=SqlSugar.DbType.SqlServer,
                IsAutoCloseConnection=true,
                InitKeyType=InitKeyType.SystemTable
            });
            return Db;
        }
        #region 查询相关
        //单表查询
        public static ISugarQueryable<T> Queryable<T>() where T : class, new()
        {
            return GetSqlSugarClient().Queryable<T>();
        }
        //多表查询
        public static ISugarQueryable<T, T2> Queryable<T, T2>(System.Linq.Expressions.Expression<Func<T, T2, object[]>> joinExpression) where T : class, new()
        {
            return GetSqlSugarClient().Queryable<T, T2>(joinExpression);
        }
        public static ISugarQueryable<T, T2, T3> Queryable<T, T2, T3>(System.Linq.Expressions.Expression<Func<T, T2, T3, object[]>> joinExpression) where T : class, new()
        {
            return GetSqlSugarClient().Queryable<T, T2, T3>(joinExpression);
        }

        public static ISugarQueryable<T, T2, T3, T4> Queryable<T, T2, T3, T4>(System.Linq.Expressions.Expression<Func<T, T2, T3, T4, object[]>> joinExpression) where T : class, new()
        {
            return GetSqlSugarClient().Queryable<T, T2, T3, T4>(joinExpression);
        }

        public static ISugarQueryable<T, T2, T3, T4, T5> Queryable<T, T2, T3, T4, T5>(System.Linq.Expressions.Expression<Func<T, T2, T3, T4, T5, object[]>> joinExpression) where T : class, new()
        {
            return GetSqlSugarClient().Queryable<T, T2, T3, T4, T5>(joinExpression);
        }

        public static ISugarQueryable<T, T2, T3, T4, T5, T6> Queryable<T, T2, T3, T4, T5, T6>(System.Linq.Expressions.Expression<Func<T, T2, T3, T4, T5, T6, object[]>> joinExpression) where T : class, new()
        {
            return GetSqlSugarClient().Queryable<T, T2, T3, T4, T5, T6>(joinExpression);
        }

        public static ISugarQueryable<T, T2, T3, T4, T5, T6, T7> Queryable<T, T2, T3, T4, T5, T6, T7>(System.Linq.Expressions.Expression<Func<T, T2, T3, T4, T5, T6, T7, object[]>> joinExpression) where T : class, new()
        {
            return GetSqlSugarClient().Queryable<T, T2, T3, T4, T5, T6, T7>(joinExpression);
        }

        public static ISugarQueryable<T, T2, T3, T4, T5, T6, T7, T8> Queryable<T, T2, T3, T4, T5, T6, T7, T8>(System.Linq.Expressions.Expression<Func<T, T2, T3, T4, T5, T6, T7, T8, object[]>> joinExpression) where T : class, new()
        {
            return GetSqlSugarClient().Queryable<T, T2, T3, T4, T5, T6, T7, T8>(joinExpression);
        }

        public static ISugarQueryable<T, T2, T3, T4, T5, T6, T7, T8, T9> Queryable<T, T2, T3, T4, T5, T6, T7, T8, T9>(System.Linq.Expressions.Expression<Func<T, T2, T3, T4, T5, T6, T7, T8, T9, object[]>> joinExpression) where T : class, new()
        {
            return GetSqlSugarClient().Queryable<T, T2, T3, T4, T5, T6, T7, T8, T9>(joinExpression);
        }

        public static ISugarQueryable<T, T2, T3, T4, T5, T6, T7, T8, T9, T10> Queryable<T, T2, T3, T4, T5, T6, T7, T8, T9, T10>(System.Linq.Expressions.Expression<Func<T, T2, T3, T4, T5, T6, T7, T8, T9, T10, object[]>> joinExpression) where T : class, new()
        {
            return GetSqlSugarClient().Queryable<T, T2, T3, T4, T5, T6, T7, T8, T9, T10>(joinExpression);
        }

        public static ISugarQueryable<T, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> Queryable<T, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(System.Linq.Expressions.Expression<Func<T, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, object[]>> joinExpression) where T : class, new()
        {
            return GetSqlSugarClient().Queryable<T, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(joinExpression);
        }

        public static ISugarQueryable<T, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> Queryable<T, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(System.Linq.Expressions.Expression<Func<T, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, object[]>> joinExpression) where T : class, new()
        {
            return GetSqlSugarClient().Queryable<T, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(joinExpression);
        }


        #endregion

        #region 更新相关
        //更新相关
        public static IUpdateable<T> Updateable<T>(T UpdateObj) where T : class, new()
        {
            return GetSqlSugarClient().Updateable<T>(UpdateObj);
        }
        public static IUpdateable<T> Updateable<T>() where T : class, new()
        {
            return GetSqlSugarClient().Updateable<T>();
        }
        #endregion

        #region 插入相关
        //插入相关
        public static IInsertable<T> Insertable<T>(T insertObj) where T : class, new()
        {
            return GetSqlSugarClient().Insertable<T>(insertObj);
        }
        #endregion

        #region 删除相关
        public static IDeleteable<T> Deleteable<T>() where T : class, new()
        {
            return GetSqlSugarClient().Deleteable<T>();
        }
        #endregion
    }
}
