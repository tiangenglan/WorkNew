﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GongZuo.Framework
{
   public  class WebViewEngine:RazorViewEngine
    {
        public WebViewEngine()
        {
            MasterLocationFormats = new[] { "~/Views/Shared/{0}.cshtml" };
            ViewLocationFormats = new[] {
                "~/Views/{1}/{0}.cshtml",
                "~/Views/Shared/{0}.cshtml"
            };
            PartialViewLocationFormats = ViewLocationFormats;
        }


    }
}
