﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace GongZuo.Framework
{
   public class BaseController:Controller
    {
        public Model.t_user Account
        {
            get
            {
                return (Model.t_user)System.Web.HttpContext.Current.Session["User"];
            }
        }

        public BaseController()
        {
            if (System.Web.HttpContext.Current.Session["User"] != null)
            {
                Model.t_user user = (Model.t_user)System.Web.HttpContext.Current.Session["User"];
                ViewBag.Account = user;
            }
        }
    }
}
