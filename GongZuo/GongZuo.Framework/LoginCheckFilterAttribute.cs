﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GongZuo.Common;
using GongZuo.Model;
namespace GongZuo.Framework
{
  public  class LoginCheckFilterAttribute:ActionFilterAttribute, IAuthorizationFilter
    {
        /// <summary>
        /// 表示是否检查登录
        /// </summary>
        public bool IsCheck { get; set; }

        public virtual void OnAuthorization(AuthorizationContext filterContext)
        {
            if (IsCheck)
            {
               GongZuo.Model.t_user user;
                string token = HttpContext.Current.Request["token"];
                if (!string.IsNullOrWhiteSpace(token))
                {
                    if (token.Contains("%"))
                    {
                        token = HttpUtility.UrlDecode(token);
                    }
                    try
                    {
                        string code = Common.Utils.DecryptDES(token);
                        string strid = code.Remove(code.Length - 11);
                        decimal id = decimal.Parse(strid);
                        DateTime endtime = code.Substring(code.Length - 11).ToDateTime("yyyy年MM月dd日");
                        if (DateTime.Now > endtime)
                        {
                            filterContext.Result = new ContentResult()
                            {
                                Content = "token已失效",
                                ContentEncoding = System.Text.Encoding.UTF8
                            };
                            return;
                        }
                        else
                        {
                            user = Db.Queryable<t_user>().Where(u => u.id == id).First();
                            //获取权限
                            System.Web.HttpContext.Current.Session["User"] = user;
                        }
                    }
                    catch (Exception ex)
                    {
                        user = null;
                    }
                }
                else
                {
                    user = (Model.t_user)System.Web.HttpContext.Current.Session["User"];
                }
                if (user==null)
                {
                    //判断是否是ajax 请求
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        filterContext.Result = new ContentResult()
                        {
                            Content = "nullSession",
                            ContentEncoding = System.Text.Encoding.UTF8
                        };
                        return;

                    }
                    else
                    {
                        //跳转到登陆页
                        string url = System.Web.HttpUtility.UrlEncode(HttpContext.Current.Request.Url.ToString());
                        //string url = System.Web.Mvc.UrlHelper.GenerateContentUrl("~/Account/Login", filterContext.HttpContext);
                        filterContext.HttpContext.Response.Redirect("~/Account/Logout?retUrl=" + url);
                        filterContext.Result = new ContentResult();
                        return;
                    }
                }
            }
        }
    }
}
