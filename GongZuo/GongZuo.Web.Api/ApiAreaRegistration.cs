﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GongZuo.Web.Api
{
   public  class ApiAreaRegistration: AreaRegistration
    {
        public override string AreaName
        {
            get { return "Api"; }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Api_default",
                "Api/{controller}/{action}/{id}",
                new { Controller = "Home", action = "Index", id = UrlParameter.Optional, area = "Api" },
                new[] { "GongZuo.Web.Api.Controllers" }
                );
        }
    }
}
